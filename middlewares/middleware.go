package middlewares

import "github.com/gin-gonic/gin"

func Guest(ctx *gin.Context) {
	if ctx.GetHeader("Token") == "" {
		//do something here
	}

	ctx.Next()
}
