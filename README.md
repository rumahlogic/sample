# Sample Project Microservice

---

### How to run
```shell
$ go run main.go api --host=0.0.0.0 --port=8080
```

### Struktur Project

```shell
.
├── README.md
├── apps
│   ├── example
│   │   ├── controllers
│   │   │   └── example.go
│   │   ├── init.go
│   │   ├── models
│   │   │   └── example.go
│   │   ├── repositories
│   │   │   └── example-repository.go
│   │   └── services
│   │       └── example-services.go
├── base
│   ├── base.go
│   ├── config
│   │   ├── database.go
│   │   ├── env.go
│   │   ├── middleware.go
│   │   └── modules.go
│   ├── console
│   │   └── console.go
│   └── types
│       └── slug.go
├── go.mod
├── go.sum
├── helpers
│   └── init.go
├── main.go
├── makefile
├── media
│   ├── assets
│   └── uploads
├── middleware
│   └── auth.go
├── migrations
│   ├── databases
│   │   └── init.go
│   └── seeders
│       └── init.go
├── modules
│   └── mod.go
├── routers
│   └── urls.go
└── templates
    ├── html
    └── json


```

### Deskripsi

1. Folder `apps` berisi module - module (service) yang berhubungan dengan aplikasi yang akan dibuat. Setiap module yang
   dibuat harus mengikuti arsitektur yang telah ditetapkan dimana service/medule memiliki beberapa layer utama
   yaitu `controllers`, `models`, `repositories`, `services`.
2. Folder `base` berisi fungsi utama untuk menjalankan applikasi ini. Folder ini terdiri dari `config`, `console`,
   dan `types`. `config` berisi konfigurasi untuk keseluruhan project, `console` berisi perintah untuk menjalanjan
   server atau dapat ditambahkan dengan beberapa fungsi lain jika dibutuhkan, `types` bersifat opsional dimana berisi
   tipe data kostum yang tidak tersedia di golang.
3. Folder `helper` berisi fungsi-fungsi bantuan yang dapat digunakan untuk semua module.
4. Folder `media` terdiri dari folder `assets` dan `uploads` dimana folder `assets` digunakan untuk menyimpan file-file
   statik yang dibutuhkan aplikasi dan folder `uploads` berisi file-file dinamis yang menampung unggahan dari pengguna.
5. Folder `middlewares` berisi fungsi-fungsi middleware yang bersifat global yang dapat digunakan untuk semua module.
6. Folder `modules` berisi abstrak atau object module untuk dapat meregister semua module yang ada di folder `apps`.
7. Folder `routers` berisi fungsi untuk menjalankan web server dan membuat route untuk semua handler yang diregister
   oleh module-module pada folder `apps`.
8. Folder `templates` berisikan file-file template untuk membuat antarmuka dinamis dengan response `html`.