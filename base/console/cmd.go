package console

import (
	"github.com/urfave/cli/v2" //silahkan baca" dokumentasi penggunaannya
	//Auto migrate all tables
	_ "gitlab.com/rumahlogic/sample/base/databases"

	"gitlab.com/rumahlogic/sample/routers"
	"os"
)

func init() {
	//Run app
	run()
}

func run() {
	//Get services
	var api = routers.NewServer()

	//Define app
	app := &cli.App{
		Name:        "Base Monolithic",
		Description: "Base Monolithic",
		Copyright:   "Copyright © 2023",
		Version:     "v1",
		Commands: []*cli.Command{
			//Running API
			{
				Name:        "api",
				Description: "Running main server engine",
				Usage:       "./main api [command options] [arguments...]",
				Flags: []cli.Flag{
					&cli.StringFlag{
						Name:        "host",
						Value:       "0.0.0.0",
						Usage:       "./main api --host 0.0.0.0",
						Required:    true,
						Destination: &api.Host,
					},
					&cli.IntFlag{
						Name:        "port",
						Value:       3000,
						Usage:       "./main api --port 3000",
						Required:    true,
						Destination: &api.Port,
					},
				},
				Action: func(ctx *cli.Context) error {
					//Run GIN Engine
					return api.Run()
				},
			},
		},
	}

	//Run app
	err := app.Run(os.Args)
	panic(err)
}
