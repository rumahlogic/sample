package modules

import "github.com/gin-gonic/gin"

type Handler struct {
	IsPrivate    bool
	Method       string
	RelativePath string
	Middleware   gin.HandlerFunc
	Func         gin.HandlerFunc
	SubHandlers  []Handler
}

type Module struct {
	Name       string
	Middleware []gin.HandlerFunc
	Handlers   []Handler
	Models     []interface{}
}

type Modules []Module

func New(name string) Module {
	return Module{
		Name: name,
	}
}

func (this *Module) RegisterModels(models ...interface{}) {
	this.Models = models
}

func (this *Module) RegisterHandlers(handlers ...Handler) {
	this.Handlers = handlers
}

func (this *Module) RegisterMiddlewares(middlewares ...gin.HandlerFunc) {
	this.Middleware = middlewares
}
