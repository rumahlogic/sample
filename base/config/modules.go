package config

import (
	"gitlab.com/rumahlogic/sample/apps/sample"
	"gitlab.com/rumahlogic/sample/base/modules"
)

var MODULES = modules.Modules{

	/*
	 * Package Config Modules...
	 */
	sample.Module,
}
