package config

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/rumahlogic/sample/middlewares"
)

type Middleware []gin.HandlerFunc

// MIDDLEWARE for register all middle
var MIDDLEWARE = Middleware{

	/*
	 * Package Config Middleware...
	 */

	middlewares.Guest,
}
