package models

import "gorm.io/gorm"

type Sample struct {
	gorm.Model
	Name string `json:"name"`
	Age  int    `json:"age"`
}
