package sample

import (
	"gitlab.com/rumahlogic/sample/apps/sample/controllers"
	"gitlab.com/rumahlogic/sample/apps/sample/middlewares"
	"gitlab.com/rumahlogic/sample/apps/sample/models"
	"gitlab.com/rumahlogic/sample/base/modules"
	"net/http"
)

var Module = modules.New("sample")

func init() {
	//Register models of this module
	Module.RegisterModels(
		//Add models here
		models.Sample{},
	)

	//Register handlers of this module
	Module.RegisterHandlers(
		// Add handlers here
		modules.Handler{
			Method:       http.MethodGet,
			RelativePath: "/get",
			Func:         controllers.Sample{}.Get,
		},
		modules.Handler{
			Method:       http.MethodPost,
			RelativePath: "/create",
			Middleware:   middlewares.Sample, //contoh jika menggunakan middleware per endpoint
			Func:         controllers.Sample{}.Create,
		},
		modules.Handler{
			Method:       http.MethodPut,
			RelativePath: "/update",
			Func:         controllers.Sample{}.Update,
		},
		modules.Handler{
			Method:       http.MethodDelete,
			RelativePath: "/delete",
			Func:         controllers.Sample{}.Delete,
		},
	)

	//Register middlewares of this module
	Module.RegisterMiddlewares(
		//Add middleware here
		middlewares.Sample, // contoh moddleware per module
	)
}
