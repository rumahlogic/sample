package controllers

import (
	"github.com/gin-gonic/gin"
	"net/http"
)

type Sample struct {
}

func (this Sample) Get(ctx *gin.Context) {

	//do something here

	ctx.SecureJSON(http.StatusOK, gin.H{
		"code":     "sample_get",
		"is_error": false,
		"message":  "success",
		"data":     []string{"sample", "success"},
	})
}

func (this Sample) Create(ctx *gin.Context) {

	//do something here

	ctx.SecureJSON(http.StatusOK, gin.H{
		"code":     "sample_create",
		"is_error": false,
		"message":  "success",
		"data":     []string{"sample", "success"},
	})
}

func (this Sample) Update(ctx *gin.Context) {

	//do something here

	ctx.SecureJSON(http.StatusOK, gin.H{
		"code":     "sample_update",
		"is_error": false,
		"message":  "success",
		"data":     []string{"sample", "success"},
	})
}

func (this Sample) Delete(ctx *gin.Context) {

	//do something here

	ctx.SecureJSON(http.StatusOK, gin.H{
		"code":     "sample_delete",
		"is_error": false,
		"message":  "success",
		"data":     []string{"sample", "success"},
	})
}
