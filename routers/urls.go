package routers

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"gitlab.com/rumahlogic/sample/base/config"
	"gitlab.com/rumahlogic/sample/base/modules"
	"gopkg.in/olahol/melody.v1"
	"net/http"
	"strings"
)

type App struct {
	Host string
	Port int

	Http      *gin.Engine
	Websocket *melody.Melody
}

func NewServer() *App {
	return &App{
		Http:      gin.Default(),
		Websocket: melody.New(),
	}
}

func readSubGroup(app *gin.RouterGroup, handler modules.Handler) {

	// Handler have sub handler
	if len(handler.SubHandlers) > 0 {
		var group = app.Group(handler.RelativePath, []gin.HandlerFunc{handler.Middleware}...)
		for _, subHandler := range handler.SubHandlers {
			readSubGroup(group, subHandler)
			if handler.Func == nil {
				return
			}
		}
	}

	var handlers []gin.HandlerFunc

	if handler.Middleware != nil {
		handlers = append(handlers, handler.Middleware)
	}
	handlers = append(handlers, handler.Func)

	switch handler.Method {
	case http.MethodGet:
		app.GET(handler.RelativePath, handlers...)
	case http.MethodPost:
		app.POST(handler.RelativePath, handlers...)
	case http.MethodPut:
		app.PUT(handler.RelativePath, handlers...)
	case http.MethodDelete:
		app.DELETE(handler.RelativePath, handlers...)
	default:
		app.Any(handler.RelativePath, handlers...)
	}

}

func (app *App) Read(modules *modules.Modules) {
	for _, mod := range *modules {
		group := app.Http.Group(strings.ToLower(mod.Name), mod.Middleware...)
		{
			for _, handler := range mod.Handlers {
				readSubGroup(group, handler)
			}
		}

	}
}

func (app *App) Run() (err error) {

	//Set global middleware here
	app.Http.Use(config.MIDDLEWARE...)

	//Read all modules handler
	app.Read(&config.MODULES)

	//TODO: add websocket

	return app.Http.Run(fmt.Sprintf("%s:%d", app.Host, app.Port))
}
